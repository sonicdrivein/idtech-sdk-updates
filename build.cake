#tool nuget:?package=NUnit.ConsoleRunner&version=3.8.0
#tool nuget:?package=NUnit.Extension.NUnitV2ResultWriter&version=3.6.0
using System.Diagnostics;

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// ENVIRONMENT VARIABLES
//////////////////////////////////////////////////////////////////////

var version = EnvironmentVariable("GO_PIPELINE_LABEL") ?? "0.0.0.0";

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define directories.
var buildDir = Directory("./DotNet-Reference-App/bin") + Directory(configuration);

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
    CleanDirectories(string.Format("./**/obj/{0}", configuration));
    CleanDirectories(string.Format("./**/bin/{0}", configuration));
});

Task("Update-Assembly-Info")
    .Does(() =>
    {
        var file = "./SharedAssemblyInfo.cs";
        CreateAssemblyInfo(file, new AssemblyInfoSettings
        {
            Product = "DotNet-Reference-App",
            Version = version,
            FileVersion = version,
            InformationalVersion = version,
            Copyright = string.Format("Copyright (c) Sonic Drive In 2018 - {0}", DateTime.Now.Year)
        });
});


Task("Package")
    .Does(() =>
    {
         FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(@"./bin/IDTechSDK.dll");
         string fileVersion = myFileVersionInfo.FileVersion;
         System.IO.File.WriteAllText(@"./pcg_sdk_version.txt", fileVersion);
            CleanDirectories("./nuget");

         var nuGetPackSettings   = new NuGetPackSettings {
                                     Id                      = "SonicIDTechSDK",
                                     Version                 = fileVersion,
                                     Title                   = "Sonic IDTech SDK",
                                     Authors                 = new[] {"IDTech"},
                                     Owners                  = new[] {"IDTech"},
                                     Description             = "Internal IDTech SDK for Sonic",
                                     Summary                 = "Used for controlling VP6800 devices",
                                     ProjectUrl              = new Uri("https://github.com/SomeUser/TestNuGet/"),
                                     IconUrl                 = new Uri("http://cdn.rawgit.com/SomeUser/TestNuGet/master/icons/testNuGet.png"),
                                     LicenseUrl              = new Uri("https://github.com/SomeUser/TestNuGet/blob/master/LICENSE.md"),
                                     Copyright               = "Sonic",
                                     ReleaseNotes            = new [] {"Bug fixes", "Issue fixes", "Typos"},
                                     Tags                    = new [] {"Cake", "Script", "Build"},
                                     RequireLicenseAcceptance= false,
                                     Symbols                 = false,
                                     NoPackageAnalysis       = true,
                                     Files                   = new [] {
                                                                          new NuSpecContent {Source = "bin/IDTechPKI.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/IDTechSDK.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/IDTechWebSvc.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_config.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_ctls.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_device.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_emv.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_icc.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_lcd.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_msr.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_pin.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/NEO2_ws.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/PKIWeb.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/RKIUtility.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/RKIWeb.dll", Target = "lib/net40"},
                                                                          new NuSpecContent {Source = "bin/RKIWebPKISymmetric.dll", Target = "lib/net40"}
                                                                       },
                                     BasePath                = "./",
                                     OutputDirectory         = "./nuget"
                                 };

     NuGetPack(nuGetPackSettings);
    }
    );

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Package");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
