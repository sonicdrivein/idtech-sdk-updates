# IDTech SDK Repo

This repo is intended for IDTech to upload updates to device sdk. 

All dlls should be updated in the bin folder.

Once updated a pipeline will be triggered in GOCD and the dlls will be packaged and uploaded to internal Sonic Nuget Repo.

Note that the new version will not be packaged into Payment Channel Gateway automatically. 
The packages.config needs to be subsequently updated in the Payment Channel Gateway project.